import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angularPiloto2';
  valorFinal = "$0";
  tasaInteres = "0,00%";
  miFormulario = new FormGroup({
    initialValue: new FormControl('', [Validators.required]),
    interestDays: new FormControl(0, [Validators.required]),
    // Inicializa 'archivo' como null y establece el tipo de archivo a aceptar
    archivo: new FormControl<File | null>(null, [Validators.required])
  });

  onFileSelect(event: Event) {
    const eventTarget = event.target as HTMLInputElement;
    // Verifica si event.target y files existen
    if (eventTarget && eventTarget.files && eventTarget.files.length) {
      const file = eventTarget.files[0];
      // Asegúrate de que file no es undefined
      if (file) {
        this.miFormulario.patchValue({ archivo: file });
        // Usa el operador '!' para asegurar a TypeScript que el objeto no es null
        this.miFormulario.get('archivo')!.updateValueAndValidity();
      }
    }
  }

  onSubmit() {
    if (this.miFormulario.invalid) {
      return;
    }

    // Aquí iría la lógica para manejar los datos del formulario,
    // como enviarlos a un servidor.
    console.log(this.miFormulario.value);

    if (this.miFormulario.value.initialValue !== null && this.miFormulario.value.initialValue !== undefined) {
      this.valorFinal  = this.miFormulario.value.initialValue;
    }
  }
}
